#pragma once

#include "Messenger/Listener.h"

class CWindowZoom : public CListener
{
public:
					CWindowZoom();
					~CWindowZoom();
	virtual void	Push( CMessage& rMessage );
};
