#include "WindowZoom.h"
#include <algorithm>
#include "Windows Input/MouseMessage.h"
#include "Messenger/Messenger.h"
#include "Window/Window.h"

CWindowZoom::CWindowZoom()
{
	CMessenger::GlobalListen( *this, CMouseMessage::GetMouseMessageUID() );
}

CWindowZoom::~CWindowZoom()
{
	CMessenger::GlobalStopListening( *this, CMouseMessage::GetMouseMessageUID() );
}

void CWindowZoom::Push(CMessage& rMessage)
{
	if( rMessage.GetType() == CMouseMessage::GetMouseMessageUID() )
	{
		CMouseMessage& rMouseMessage( static_cast< CMouseMessage& >( rMessage ) );
		if (rMouseMessage.GetMouseAction() == CMouseMessage::WHEEL)
		{
			assert( CWindow::GetDefaultWindow() );
			f32 fZoom = -std::clamp(rMouseMessage.X(), -1200.f, 1200.f);
			f32 fScaleFactor = NUtilities::ConvertCoordinate(fZoom, -1200.f, 1200.f, 0.1f, 1.9f);
			CWindow::GetDefaultWindow()->ScaleBy( fScaleFactor );
		}
	}
}
